terraform {
    backend "s3" {
        bucket  = "scriptmyjob.terraform.tfstate"
        key     = "Grafana/terraform.tfstate"
        region  = "us-west-2"
        encrypt = "true"
        profile = "default"
    }
}
