data "aws_route53_zone" "selected" {
    name = "${lookup(var.route53,"domain")}"
}

resource "aws_route53_record" "grafana" {
    zone_id = "${data.aws_route53_zone.selected.zone_id}"
    name    = "${lookup(var.route53,"subdomain")}.${lookup(var.route53,"domain")}"
    type    = "A"

    alias {
        name                   = "${aws_lb.grafana.dns_name}"
        zone_id                = "${aws_lb.grafana.zone_id}"
        evaluate_target_health = true
    }
}

